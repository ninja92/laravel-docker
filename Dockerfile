FROM php:7.2-fpm
COPY src/ /var/www/html/
WORKDIR /var/www/html/

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN apt-get update && apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-configure zip --with-libzip \
  && docker-php-ext-install zip

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
